﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleJsonSerializer
{
    public static class Foreacher
    {
        public delegate void ForEachBody<TElement>(TElement element);

        public static void ForEach<TElement>(IEnumerable<TElement> elements, ForEachBody<TElement> @delegate)
        {
            var iterator = elements.GetEnumerator();
            for (; iterator.MoveNext();)
            {
                var element = iterator.Current;
                @delegate(element);
            }
        }

        public static void TestForeach()
        {
            var list = new List<string>() { "1", "22323", "sdsdsd" };
            ForEachBody<string> method = new ForEachBody<string>(Method);
            ForEach<string>(list, method);
        }

        public static void Method<TElement>(TElement element)
        {
            Console.WriteLine(element);
        }

    }
}
