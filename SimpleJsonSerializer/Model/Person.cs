﻿namespace SimpleJsonSerializer.Model
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public MyDate BirthDate { get; set; }
        public Gender Gender { get; set; }

    }
}
