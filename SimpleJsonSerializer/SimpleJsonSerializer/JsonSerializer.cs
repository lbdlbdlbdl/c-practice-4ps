﻿using SimpleJsonSerializer.Model;
using System.Reflection;
using System.Collections;
using System;
using System.Text;
using System.Linq;

namespace SimpleJsonSerializer
{
    public class JsonSerializer
    {
        private StringBuilder _outStr = new StringBuilder();

        public string Serialize(Object obj)
        {
            _outStr.Append("{ \n");
            object serializedProperty = null;
            var properties = obj.GetType().GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                var ending = (i != properties.Length - 1) ? ",\n" : "\n}";
                var propertyType = properties[i].PropertyType;
                //var propertyValue = properties[i].GetValue(obj);

                if (IsNotPrimitive(propertyType, obj))
                {
                    PutPropertyNameToJsonString(properties[i].Name);
                    var propertyValue = properties[i].GetValue(obj, null);
                    serializedProperty = this.Serialize(propertyValue);
                    if (i != properties.Length - 1)
                    {
                        _outStr.Append(",");
                    }
                }
                else if (IsCollection(propertyType))
                {
                    IList list = properties[i].GetValue(obj) as IList;
                    PutPropertyNameToJsonString(properties[i].Name);
                    _outStr.Append("[\n");
                    for (int j = 0; j < list.Count; j++)
                    {
                        serializedProperty = list[j];
                        ending = (j != list.Count - 1) ? ",\n" : ((i != properties.Length - 1) ? "\n],\n" : ending = "\n]");
                        PutPropertyValueToJsonString(list[j], ending);
                    }
                }
                else
                {
                    var propertyValue = properties[i].GetValue(obj, null);
                    serializedProperty = propertyValue;
                    PutPropertyNameToJsonString(properties[i].Name);
                    PutPropertyValueToJsonString(serializedProperty, ending);
                    //_str.AppendFormat("\"{0}\" : \"{1}\"{2}", properties[i].Name, serializedProperty, ending);
                }
            }
            return _outStr.ToString();
        }

        private bool IsNotPrimitive(Type type, Object obj)
        {
            return (type.IsClass && type.Assembly.FullName == obj.GetType().Assembly.FullName);
        }

        private bool IsCollection(Type type)
        {
            return type.GetInterfaces().Contains(typeof(IEnumerable)) && !(type == typeof(string));
        }

        private void PutPropertyNameToJsonString(string propertyName)
        {
            _outStr.AppendFormat("\"{0}\" : ", propertyName);
        }

        private void PutPropertyValueToJsonString(object propertyValue, string ending)
        {
            _outStr.AppendFormat("\"{0}\" {1}", propertyValue, ending);
        }
    }
}
