﻿using System.Collections.Generic;

namespace SimpleJsonSerializer.Model
{
    public class Employee : Person
    {
        public string Company { get; set; }

        public List<string> Projects { get; set; }

        public Employee() :base() { }
    }
}
