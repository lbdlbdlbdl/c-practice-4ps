﻿using SimpleJsonSerializer.Model;
using System;
using System.Collections.Generic;

namespace SimpleJsonSerializer
{
    class Program
    {
        static void Main(string[] args)
        {
            var json = new JsonSerializer();
            Employee employee = new Employee()
            {
                FirstName = "Alex",
                LastName = "Malinov",
                Gender = Gender.Male,
                BirthDate = new MyDate()
                {
                    Year = 1999,
                    Month = 3,
                    Day = 2
                },
                Company = "OOO",
                Projects = new List<string>()
                {
                    "FirstProj",
                    "SecondProj"
                }
            };
            var list = new List<int> { 1, 2, 3 };
            //Console.WriteLine(json.Serialize(employee));
            Console.WriteLine(json.Serialize(list));
            Console.ReadKey();
        }
    }
}
