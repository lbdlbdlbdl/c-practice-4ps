﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TransportDB.Tests
{
    [TestClass]
    public class DataBaseTests
    {
        public DataBase database;
        public Car _car1;
        public Car _car2;
        public Bicycle _bicycle1;

        [TestInitialize]
        public void Initialize()
        {
            database = new DataBase();

            _car1 = new Car()
            {
                Name = "car1",
                Brand = "brand1",
                Year = 1999,
                BodyType = "lala"
            };
            _car2 = new Car()
            {
                Name = "car1",
                Brand = "brand1",
                Year = 1999,
                BodyType = "rere"
            };
            _bicycle1 = new Bicycle()
            {
                Name = "bic1",
                Brand = "barnd of bic 1",
                Year = 1000,
                Absorber = "mountain"
            };

        }

        [TestCleanup]
        public void Clean()
        {
            database = null;
        }

        [TestMethod]
        public void AddWithUniqueId()
        {
            //act
            database.Add(_car1);
            database.Add(_car2);
            database.Add(_bicycle1);

            //assert
            Assert.AreNotEqual(_car1.ID, _car2.ID);
        }

        [TestMethod]
        public void FindNotExistId()
        {
            var id = -1;
            var obj = database.Find(id);
            Assert.AreEqual(null, obj);
        }

        [TestMethod]
        public void FindExistId()
        {
            //arrange
            var id = 1;
            database.Add(_car1);
            database.Add(_car2);

            //act
            Transport targetCar = database.Find(id);

            //assert
            Assert.AreEqual(_car1, targetCar);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ChangeNameNotExistId()
        {
            //arrange
            var id = -1;
            var name = "Mashina";

            //act
            database.ChangeName(id, p => p.Brand = name);
        }

        [TestMethod]
        public void ChangeNameExistId()
        {
            //arrange
            database.Add(_car1);
            database.Add(_car2);

            //act
            database.ChangeName(1, p => p.Brand = "Mashina");

            //assert
            Assert.AreEqual(_car1.Brand, "Mashina");
        }

        [TestMethod]
        public void DeleteByNotExistId()
        {
            //arrange
            var id = 45;
            database.Add(_car1);
            database.Add(_car2);

            //act
            var isDeleted = database.Delete(id);
            Assert.AreEqual(false, isDeleted);
        }

        [TestMethod]
        public void DeleteByExistId()
        {
            //arrange
            database.Add(_car1);
            database.Add(_car2);
            database.Add(_bicycle1);
            var isDeleted = database.Delete(1);
            var obj = database.Find(1);
            Assert.AreEqual(null, obj);

        }




    }
}
