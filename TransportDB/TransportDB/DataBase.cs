﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace TransportDB
{
    [DataContract]
    public class DataBase
    {
        [DataMember]
        private List<Transport> _items = new List<Transport>();
        [DataMember]
        private int _nextId;

        private Serializer _serializer = new Serializer();

        public DataBase()
        {
            _items = new List<Transport>();
            _nextId = 0;
        }
        
        public void Load()
        {
            var db = _serializer.Deserialize();
            _items = db._items;
            _nextId = db._nextId;
        }

        public void Save()
        {
            _serializer.Serialize(this);
        }

        public void Add(Transport item)
        {
            item.ID = _nextId + 1;
            _nextId = item.ID;
            _items.Add(item);
        }
        

        public bool Delete(int id)
        {
            try
            {
                var isDeleted = _items.Remove(Find(id));
                return isDeleted;
            }
            catch
            {
                throw new Exception();
            }
        }

        public Transport Find(int id) => _items.SingleOrDefault(x => id == x.ID);

        public void ChangeName(int id, Func<Transport, object> prop)
        {
            try
            {
                var obj = Find(id);
                var propValue = prop(obj);
            }
            catch
            {
                throw new ArgumentException();
            }
        }

        public IEnumerable<Transport> All
        {
            get
            {
                return _items.ToArray();
            }
        }
    }
}
