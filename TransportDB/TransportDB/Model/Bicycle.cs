﻿using System.Runtime.Serialization;

namespace TransportDB
{
    [DataContract]
    public class Bicycle : Transport
    {
        [DataMember]
        public string Absorber { get; set; }

        public override string Show()
        {
            return $"ID: {ID} | Name: {Name} | Brand: {Brand} | Year: {Year} | Absorber: {Absorber}";
        }


        public Bicycle() : base() { }
    }
}
