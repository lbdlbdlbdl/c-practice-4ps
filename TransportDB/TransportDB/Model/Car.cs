﻿using System.Runtime.Serialization;

namespace TransportDB
{
    [DataContract]
    public class Car : Transport
    {
        [DataMember]
        public string BodyType { get; set; }

        public override string Show()
        {
            return $"ID: {ID} | Name: {Name} | Brand: {Brand} | Year: {Year} | BodyType: {BodyType}";
        }

        public Car() : base() { }

    }
}
