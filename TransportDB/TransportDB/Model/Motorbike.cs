﻿using System.Runtime.Serialization;

namespace TransportDB
{
    [DataContract]
    public class Motorbike : Transport
    {
        [DataMember]
        public string Class { get; set; }

        public override string Show()
        {
            return $"ID: {ID} | Name: {Name} | Brand: {Brand} | Year: {Year} | Class: {Class}";
        }
        public Motorbike() : base() { }
    }
}
