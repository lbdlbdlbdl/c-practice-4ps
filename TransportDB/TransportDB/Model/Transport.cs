﻿using System.Runtime.Serialization;

namespace TransportDB
{
    [DataContract]
    public abstract class Transport
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Brand { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Year { get; set; }

        public abstract string Show();

        public Transport() { }

    }
}
