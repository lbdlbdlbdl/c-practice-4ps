﻿using System;
using System.Collections.Generic;

namespace TransportDB
{
    class Program
    {
        static void Main(string[] args)
        {
            DataBase db = new DataBase();
            db.Load();
            Console.WriteLine("Loaded.");
            IEnumerable<Transport> items = db.All;
            foreach (var item in items)
            {
                Console.WriteLine(item.Show());
            }
            Console.WriteLine();
            db.Add(new Car { Name = "car1", Brand = "brand1", BodyType = "bodytype1", Year = 1999 });
            Console.WriteLine("Ok1.");
            db.Add(new Motorbike { Name = "moto1", Brand = "brand1", Class = "mountains" });
            Console.WriteLine("Ok2.");
            db.Save();
            Console.WriteLine("Saved.");

            items = db.All;
            foreach(var item in items)
            {
                Console.WriteLine(item.Show());
            }

            db.Delete(2);
            Console.WriteLine("Deleted");
            items = db.All;
            foreach (var item in items)
            {
                Console.WriteLine(item.Show());
            }
            Console.ReadKey();
        }
    }
}
