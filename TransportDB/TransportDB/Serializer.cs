﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace TransportDB
{
    public class Serializer
    {

        public void Serialize(DataBase db)
        {
            using (FileStream stream = new FileStream(Path, FileMode.Create))
            {
                JsonSerializer.WriteObject(stream, db);
            }
        }

        public DataBase Deserialize()
        {
            using (FileStream stream = new FileStream(Path, FileMode.OpenOrCreate))
            {
                return (DataBase)JsonSerializer.ReadObject(stream);
            }
        }

        private string Path
        {
            get
            {
                var directory = (Directory.GetParent(Directory.GetCurrentDirectory())).Parent;
                var root = $"{directory.FullName}\\JSON";
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                return $"{root}\\transport.json";
            }
        }

        private DataContractJsonSerializer JsonSerializer
        {
            get
            {
                List<Type> knownTypeList = new List<Type>();
                knownTypeList.Add(typeof(DataBase));
                knownTypeList.Add(typeof(Car));
                knownTypeList.Add(typeof(Motorbike));
                knownTypeList.Add(typeof(Bicycle));
                return new DataContractJsonSerializer(typeof(DataBase), knownTypeList);
            }
        }
    }
}
